import React, { Component } from 'react';
import logo from './logo.svg';
import fetchJsonp from 'fetch-jsonp';


class PostalCodeSearch extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      postalCode: '',
      data: null,
      error: false
    };
    
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.renderInfo = this.renderInfo.bind(this);
    
  }
  
  handleChange = (event) => {
    const value = event.target.value;
    
    this.setState({
      postalCode: value
    });
  }
  
  handleClick = () => {
    // the opennorth API only allows uppercase postal codes with no spaces. 
    // Transforming input to match those parameters.
    const postalCode = this.state.postalCode.replace(/ /g,'').toUpperCase();
    const requestUrl = 'https://represent.opennorth.ca/postcodes/' + postalCode;
    
    fetchJsonp( requestUrl )
    .then(response => {
      return response.json();
    }).then(json => { 
      // separating out the MP from the other elected officials in the response array.
      const arr = json.representatives_centroid;
      const memberInfo = arr.filter( obj => {
        return obj.elected_office === "MP";
      });
      return memberInfo;
    }).then(memberInfo => { 
      this.setState({
        data: memberInfo,
        error: false
      });
    }).catch (err => {
      // change state.error to true upon error in fetch call.
      console.log(err.message, err);
      this.setState({
        postalCode: '',
        data: null,
        error: true
      });
    });
  }
  
  renderInfo = () => {
    const { photo_url, party_name, district_name, name, url } = this.state.data[0];

    return (
      <div className = "content">
        <a href={ url } target="#">
          <img src={photo_url} alt="member portrait" className="content--image" />
        </a>  
        <br/>
        <span> The { party_name } MP for { district_name } is <strong>{ name }</strong> </span>
        <br/>
        <span> For more information on this member of parliament please visit <a href={ url } target="#">their website.</a></span>
      </div>
    );
  }
  
  render () {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React Tech Challenge</h1>
        </header>
        <p className="App-intro">
          Enter your postal code to find your MP!
          <br />
          <input 
            onChange={this.handleChange}
            value={this.state.postalCode}
          />
          <br />
          {/* display error if state.error is true */}
          {this.state.error && 
            <span className="error"> Oops! It looks like that postal code isn't valid! Please enter a valid postal code. </span>
          }
          <br />
          <button onClick={this.handleClick}>Go</button>
        </p>
        {/* populate renderInfo upon this.state.data having data. */}
        {this.state.data && this.renderInfo() }
      </div>
    );
  }
}

export default PostalCodeSearch;